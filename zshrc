if [ "$TERM" = "xterm" ]; then
    export TERM=xterm-256color
fi
if [ "$TERM" = "screen" -o "$TERM" = "screen-256color" ]; then
    export TERM=screen-256color
    unset TERMCAP
fi

HISTFILE=$HOME/.zsh_history
HISTFILESIZE=500
HISTSIZE=500

autoload -Uz compinit promptinit
autoload -Uz colors && colors
compinit
promptinit

zstyle ':completion:*' menu select

setopt COMPLETE_ALIASES

#automatic terminal reset
ttyctl -f

DIRSTACKFILE="$HOME/.cache/zsh/dirs"
if [[ -f $DIRSTACKFILE ]] && [[ $#dirstack -eq 0 ]]; then
  dirstack=( ${(f)"$(< $DIRSTACKFILE)"} )
  [[ -d $dirstack[1] ]] && cd $dirstack[1]
fi
chpwd() {
  print -l $PWD ${(u)dirstack} >$DIRSTACKFILE
}

DIRSTACKSIZE=20

setopt AUTO_PUSHD PUSHD_SILENT PUSHD_TO_HOME

## Remove duplicate entries
setopt PUSHD_IGNORE_DUPS

## This reverts the +/- operators.
setopt PUSHD_MINUS

#archey3

export PATH="${PATH}:/home/shin/sw-cli-tools/bin/"

alias wipe-cache-dotfiles="find ~/dotfiles -name '.swp' -delete"
alias syu="sudo pacman -Syu"

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
 . /usr/lib/python3.6/site-packages/powerline/bindings/zsh/powerline.zsh


